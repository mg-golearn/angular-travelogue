import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ProfileComponent } from './components/profile/profile.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ValidateService } from './services/validate.service'
import { FlashMessagesModule, FlashMessagesService } from 'angular2-flash-messages';
import { HttpClientModule } from '@angular/common/http'
import { AuthService } from './services/auth.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AuthGuard } from './guards/auth.guards';
import { MaterialModule } from './material.module';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { MatInputModule } from '@angular/material/input';
import { GalleryDetailsComponent } from './components/gallery-details/gallery-details.component';
import { TravelComponent } from './components/travel/travel.component';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { NgxImageSwiperModule } from 'ngx-image-swiper';
import { CurrentWeatherComponent } from './components/current-weather/current-weather.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatCardModule } from '@angular/material/card';
import { EventsComponent } from './components/events/events.component';

const appRoutes: Routes = [
  {path: '', component: HomeComponent },
  {path: 'register', component: RegisterComponent },
  {path: 'login', component: LoginComponent },
  {path: 'dashboard', component: DashboardComponent, canActivate:[AuthGuard] },
  {path: 'profile', component: ProfileComponent, canActivate:[AuthGuard] },
  {path: 'gallery', component: GalleryDetailsComponent, canActivate:[AuthGuard] },
  {path: 'addTravel', component: TravelComponent, canActivate:[AuthGuard] },
  {path: 'travel/:id', component: TravelComponent, canActivate:[AuthGuard] }
]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DashboardComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    ProfileComponent,
    GalleryDetailsComponent,
    TravelComponent,
    CurrentWeatherComponent,
    EventsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    FlashMessagesModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    MaterialModule,
    ReactiveFormsModule,
    MaterialFileInputModule,
    GooglePlaceModule,
    MatInputModule,
    NgxImageSwiperModule,
    ScrollingModule,
    MatCardModule
  ],
  providers: [
    ValidateService,
    FlashMessagesService,
    AuthService,
    AuthGuard,
    JwtHelperService,
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
