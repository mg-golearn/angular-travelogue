import { Injectable, InjectionToken } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { JwtHelperService } from '@auth0/angular-jwt';

const usersUrl = '/users'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private jwtHelperService = new JwtHelperService()
  authToken: any
  user: any
  _id: any

  constructor(private http: HttpClient) { }

  registerUser(user) {
    const headers = new HttpHeaders()
          .set('Content-Type', 'application/json')
    return this.http.post(usersUrl+'/register',
    user, {headers: headers}).pipe()
  }

  authenticateUser(user) {
    this.user = user
    const headers = new HttpHeaders()
          .set('Content-Type', 'application/json')
    return this.http.post(usersUrl+'/authenticate',
    user, {headers: headers}).pipe()
  }

  getProfile() {
    this.loadToken()
    const headers = new HttpHeaders()
          .set('Authorization', this.authToken)
          .set('Content-Type', 'application/json')
    return this.http.get(usersUrl+'/profile', {headers: headers}).pipe()
  }

  checkPassword(password, _id) {
    const headers = new HttpHeaders()
      .set('Authorization', this.authToken)
      .set('Content-Type', 'application/json')
    const params = {
      id: _id,
      password: password
    }
    return this.http.post(usersUrl+'/checkPwd', params, {headers: headers}).pipe()
  }

  updatePassword(newPassword, _id) {
    const headers = new HttpHeaders()
          .set('Authorization', this.authToken)
          .set('Content-Type', 'application/json')
    const params = {
      id: _id,
      password: newPassword
    }
    return this.http.put(usersUrl+'/updatePassword/'+_id,
          params, {headers: headers}).pipe()
  }

  updateScribble(scribble, _id){
    const headers = new HttpHeaders()
          .set('Authorization', this.authToken)
          .set('Content-Type', 'application/json')
    const params = {
      id: _id,
      scribble: scribble
    }
    return this.http.put(usersUrl+'/updateInfo/'+_id,
          params, {headers: headers}).pipe()
  }

  storeUserData(token, user){
    localStorage.setItem('id_token', token)
    localStorage.setItem('user', JSON.stringify(user))
    localStorage.setItem('_id', user.id)
    this.authToken = token
    this.user = user
    this._id = user.id
  }

  isTokenExpired(){
    return this.jwtHelperService.isTokenExpired(localStorage.getItem('id_token'))
  }

  loadToken(){
    const token = localStorage.getItem('id_token')
    const _id = localStorage.getItem('_id')
    this.authToken = token
    this._id = _id
  }

  getToken(): string {
    return this.authToken
  }

  getId(): string {
    return this._id
  }

  logout(){
    this.authToken = null
    this.user = null
    this._id = null
    localStorage.clear()
  }
}
