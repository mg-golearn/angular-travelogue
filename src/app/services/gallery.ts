export class Gallery {
    _id: string
    imageUrl: string
    travelId: string
    imageDesc: string
    uploaded: Date
}