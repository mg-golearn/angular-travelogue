import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { Gallery } from './gallery';

const galleryUrl = '/gallery'

@Injectable({
  providedIn: 'root'
})
export class GalleryService {

  constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse): any {
    if(error.error instanceof ErrorEvent) {
      console.error('An error occurred: ', error.error.message)
    }else{
      console.error(`Backend returned code ${error.status}` + 
                    `bosy was: ${error.error}`)
    }
    return throwError(`Something happened, please try again`)
  }

  getGalleryByTravelId(id: string): Observable<any> {
    const url = galleryUrl+"/travelImages/"+id
    return this.http.get<Gallery>(url).pipe()
  }

  addGallery(gallery: Gallery, travelId: string, file: File): Observable<any> {
   const formData = new FormData()
    formData.append('file', file)
    formData.append('travelId', travelId)
    formData.append('imageDesc', gallery.imageDesc)
    const header = new HttpHeaders()
    const params = new HttpParams()

    const options = {
      params,
      reportProgress: true,
      headers: header
    }

    const req = new HttpRequest('POST', galleryUrl+'/uploadImage', formData, options)
    return this.http.request(req)
  }
}
