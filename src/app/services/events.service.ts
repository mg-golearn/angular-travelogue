import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Client } from 'predicthq';

const accessToken: string = environment.accessToken
const client = new Client({access_token: accessToken});
@Injectable({
  providedIn: 'root'
})

export class EventsService {
  url = "https://api.predicthq.com/v1/"
  constructor(private httpClient : HttpClient) { }

  getPlace(loc) {
    const place = loc.substring(0, loc.indexOf(','))
    console.log("place " + place);
    
    const headers = new HttpHeaders()
          .set('Authorization', accessToken)
          .set('Accept', 'application/json')
    
    const params = new HttpParams()
          .set('q', place)

      const options = {
        params,
        headers: headers
      }
    return this.httpClient.get(this.url+'places', options).pipe()
  }

  getEvents(loc) {
    const place = loc.substring(0, loc.indexOf(','))
    console.log("place " + place);
    client.events.search({q: place})
    .then(
        (results) => {
            for (const event of results) {
                console.info(event);
                return event
            }
        }
    ).catch(
        err => console.error(err)
    );
  }
}
