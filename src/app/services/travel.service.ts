import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Travel } from '../models/travel'
import { AuthService } from './auth.service';

const travelUrl = '/travel'

@Injectable({
  providedIn: 'root'
})
export class TravelService {
  
  constructor(private httpClient: HttpClient,
    private authService: AuthService) { }

  addTravel(travel) {
    const headers = new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Authorization', this.authService.getToken())
    return this.httpClient.post(travelUrl+'/add',
    travel, {headers: headers}).pipe()
  }

  getTravelsByUserId(userId: string) {
    const headers = new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Authorization', this.authService.getToken())
    return this.httpClient.get(travelUrl+'/user/'+userId, {headers: headers}).pipe()
  }

  getTravelsById(id: string) {
    const headers = new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Authorization', this.authService.getToken())
    return this.httpClient.get(travelUrl+'/'+id, {headers: headers}).pipe()
  }

  updateTravel(id: string, updatedTravel: Travel){
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', this.authService.getToken())
    return this.httpClient.put(travelUrl+'/'+id, updatedTravel, {headers: headers}).pipe()
  }
}
