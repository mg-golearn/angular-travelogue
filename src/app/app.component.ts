import { AfterViewInit, Component } from '@angular/core';
import { environment } from 'src/environments/environment';

const placesKey = environment.placeskey
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  title = 'Travelogue';

   
  ngAfterViewInit() {
    // Load google maps script after view init
    const DSLScript = document.createElement('script');
    DSLScript.src = 'https://maps.googleapis.com/maps/api/js?key='+placesKey+'&libraries=places&language=en'; // replace by your API key
    DSLScript.type = 'text/javascript';
    document.body.appendChild(DSLScript);
    document.body.removeChild(DSLScript);
    console.log("autocomple after view init");
  }
}
