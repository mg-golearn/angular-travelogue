export class Travel {
    _id: string
    destination: string
    title: string
    startDate: string
    endDate: string
    description: string
    userId: string
    heroImg: string
}