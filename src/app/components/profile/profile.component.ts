import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  oldPassword: String
  newPassword: String
  confirmPassword: String
  scribble: String
  res: any
  user: any

  constructor(private authService: AuthService,
    private router: Router,
    private flashMessagesService: FlashMessagesService) { }

    onChangePasswordSubmit(): void {
      this.authService.getProfile().subscribe(data => {
        this.res = data
        this.user = this.res.user
        const currPassword = this.user.password  
        
        this.authService.checkPassword(this.oldPassword, this.user._id).subscribe(
          data => {
            this.res=data
            if (this.res.success) {
              if (this.newPassword !== this.confirmPassword) {
                this.flashMessagesService.show('Password mismatch! Your new password and confirm password do not match',
                    {cssClass: 'alert alert-dismissible alert-primary', timeout: 3000})
                    this.router.navigate(['/profile'])
              } else {
                //if old password is correct and newPassword is same as confirmPassword
                const user = {
                  password: this.newPassword
                }
                this.authService.updatePassword(user.password, this.user._id).subscribe(
                  data => {
                    this.res=data
                    if (this.res.success) {
                      this.flashMessagesService.show('Password updated successfully.',
                      {cssClass: 'alert alert-dismissible alert-success', timeout: 3000})
                      this.router.navigate(['/profile'])
                    } else {
                      this.flashMessagesService.show('Password could not be updated. Please try again later.',
                      {cssClass: 'alert alert-dismissible alert-primary', timeout: 3000})
                      this.router.navigate(['/profile'])
                    }
                })
              } 
            } else {
              this.flashMessagesService.show('Your old password is not correct.',
                    {cssClass: 'alert alert-dismissible alert-primary', timeout: 3000})
                    this.router.navigate(['/profile'])
            }
          },
          err => {
            this.res = err
            return false
          })
          },
          err => {
            this.res = err
            return false
          }
        )
    }

    onScribbleSubmit(): void {
      this.authService.updateScribble(this.user.scribble, this.user._id).subscribe(
            data => {
        this.res=data
        if (this.res.success) {
          this.flashMessagesService.show('Updated info successfully.',
          {cssClass: 'alert alert-dismissible alert-success', timeout: 3000})
          this.router.navigate(['/profile'])
        } else {
          this.flashMessagesService.show('Info could not be updated. Please try again later.' + this.res.msg,
          {cssClass: 'alert alert-dismissible alert-primary', timeout: 3000})
          this.router.navigate(['/profile'])
        }
      },
      err => {
        this.res = err
        return false
      })
    }

  ngOnInit(): void {
    this.authService.getProfile().subscribe(data => {
      this.res = data
      this.user = this.res.user
    },
    err => {
      this.res = err
      return false
    })
  }
}