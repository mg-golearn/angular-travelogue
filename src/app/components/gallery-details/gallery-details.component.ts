import { AutofillMonitor } from '@angular/cdk/text-field';
import { AfterViewInit, Component, OnChanges, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSwiperConfig } from 'ngx-image-swiper';
import { Gallery } from 'src/app/services/gallery';
import { GalleryService } from 'src/app/services/gallery.service';
import { TravelService } from 'src/app/services/travel.service';

//Error when invalid control is dirty, touched or submitted
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null) : boolean {
    const isSubmitted = form && form.submitted
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted))
  }
}
@Component({
  selector: 'app-gallery-details',
  templateUrl: './gallery-details.component.html',
  styleUrls: ['./gallery-details.component.css']
})
export class GalleryDetailsComponent implements OnInit {
  galleryForm: FormGroup
  imageFile: File = null
  imageDesc = ''
  travelId = ''
  isLoadingResults = true
  matcher = new MyErrorStateMatcher()
  photo: Gallery = {
    _id: '',
    travelId: '',
    imageUrl: '',
    imageDesc: '',
    uploaded: null
  }
  travelGallery: Gallery[]
  routeSub: any
  swiperConfig: NgxSwiperConfig = {
    navigation: true,
    navigationPlacement: 'inside',
    pagination: true,
    paginationPlacement: 'outside',
    imgBackgroundSize: 'contain'
  }
  images: string[] = []

  constructor(private gallery: GalleryService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private travelService: TravelService) { }

  ngOnInit(): void {
    this.galleryForm = this.formBuilder.group({
      imageFile: ['', Validators.required],
      imageDesc: ['', Validators.required]
    })
    this.routeSub = this.route.params.subscribe(params => {
      this.travelId = params['id']
      if (params['id']) {
        this.getGalleryDetails(params['id']);
      }
    })
  }

  onFormSubmit(): void {
    this.isLoadingResults = true
    this.gallery.addGallery(this.galleryForm.value, this.travelId,
      this.galleryForm.get('imageFile').value._files[0])
      .subscribe((res: any) => {
        this.isLoadingResults = false
        if(res.body) {
          this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
            this.router.navigate(['/travel/'+this.travelId]));
        }
      }, (err: any) => {
        console.log(err);
        this.isLoadingResults = false
    })
  }

  getGalleryDetails(id: string): void {
    if (id) {
      this.gallery.getGalleryByTravelId(id).subscribe((data: any) => {
        if (data) {
          this.travelGallery = data
          //console.log(this.travelGallery);
          this.travelGallery.forEach(g => {
            this.images.push(g.imageUrl)
          })
          this.isLoadingResults=false
        }
      })
    }
  }
}