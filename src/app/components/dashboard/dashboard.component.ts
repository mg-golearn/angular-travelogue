import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSwiperConfig } from 'ngx-image-swiper';
import { Travel } from 'src/app/models/travel';
import { AuthService } from 'src/app/services/auth.service';
import { Gallery } from 'src/app/services/gallery';
import { GalleryService } from 'src/app/services/gallery.service';
import { TravelService } from 'src/app/services/travel.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit{
  id: string
  galleryForm: FormGroup
  isLoadingResults = true
  travel: Travel = {
    _id: '',
    title: '',
    destination: '',
    startDate: '',
    endDate: '',
    description: '',
    userId: '',
    heroImg: ''
  }
  myTravels: Travel[]
  travelGallery: Gallery[]
  searchString: string
  filteredTravels: Travel[]
  photodata: any[]

constructor(private router: Router,
  private travelService: TravelService,
  private authService: AuthService,
  private galleryService: GalleryService) {}

  ngOnInit(): void {
    this.getTravels(this.authService.getId());
  }

  onEditSubmit(id: string) {
    //navigate to a specific travel details page where you can edit 
    //details
    this.router.navigate(['/travel/'+id])
  }

  getTravels(id: string): void {
    //Get All travels by the user logged in. The id here is the userId
    //who created the travel
    if (id) {
      this.travelService.getTravelsByUserId(id).subscribe((data: any) => {
        this.myTravels = data
        this.filteredTravels = data
        this.filteredTravels.forEach(t => {
          if(t.startDate){
            t.startDate = new Date(t.startDate).toLocaleDateString()
            if(t.endDate){
              t.endDate = new Date(t.endDate).toLocaleDateString()
            }else{
              t.endDate = ''
            } 
          }else{
            t.startDate = ''
            t.endDate = ''
          }
          
          this.galleryService.getGalleryByTravelId(t._id).subscribe((data: any) => {
            this.photodata = data
            if(data && data[0]){
              //pick first and extract image
              t.heroImg = this.photodata[0].imageUrl           
            }
          }
          )
        })
        this.isLoadingResults=false
      })
    }
  }

  searchTravel() {
    if(this.searchString) {
      this.filteredTravels = this.myTravels.filter(t => {
        if (t.title){
          if (t.destination) {
            return (t.title.toLowerCase().indexOf(this.searchString.toLowerCase()) !== -1) || 
            (t.destination.toLowerCase().indexOf(this.searchString.toLowerCase()) !== -1)
          }else {
            return (t.title.toLowerCase().indexOf(this.searchString.toLowerCase()) !== -1) 
          }
        }
      })
    }else {
      this.filteredTravels = this.myTravels
    }  
  }
}