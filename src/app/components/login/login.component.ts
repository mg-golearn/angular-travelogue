import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: String
  password: String
  res: any
  constructor(private authService: AuthService,
    private router: Router,
    private flashMessagesService: FlashMessagesService) { }

  ngOnInit(): void {
  }

  onLoginSubmit() {
    const user = {
      username: this.username,
      password: this.password
    }
    this.authService.authenticateUser(user).subscribe(data => {
      this.res=data
      if (this.res.success) {
        this.authService.storeUserData(this.res.token, this.res.user);
        //this.flashMessagesService.show('Logged in as ' + this.res.user.username,
        //{cssClass: 'alert-success', timeout: 3000})
        this.router.navigate(['/dashboard'])
      } else {
        this.flashMessagesService.show('Authentication failure. ' + this.res.msg,
        {cssClass: 'alert alert-dismissible alert-primary', timeout: 3000})
        this.router.navigate(['/login'])
      }
    })
  }
}
