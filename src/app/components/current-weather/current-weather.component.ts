import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { WeatherService } from 'src/app/services/weather.service';
@Component({
  selector: 'app-current-weather',
  templateUrl: './current-weather.component.html',
  styleUrls: ['./current-weather.component.css']
})

export class CurrentWeatherComponent implements OnInit {
  
  currentWeather: any = <any>{};
  @Input() locName: string
  msg: string;
  constructor(
    private weatherService: WeatherService
  ) {}

  ngOnInit() {
    if(this.locName) {
      this.searchWeather(this.locName)
    }
  }

  searchWeather(loc: string) {
    this.msg = '';
    this.currentWeather = {};
    this.weatherService.getCurrentWeather(loc)
      .subscribe(res => {
        this.currentWeather = res;
      }, err => {
        if (err.error && err.error.message) {
          this.msg = err.error.message;
          console.log(this.msg + " for " + this.locName);
          //try again
          const tryWithLoc = this.locName.substring(this.locName.indexOf(',') + 1).trim()
          this.weatherService.getCurrentWeather(tryWithLoc).subscribe(res => {
            this.currentWeather = res
          }, err => {
            if (err.error && err.error.message) {
              this.msg = err.error.message;
              return
            }
          })  
        }
      }, () => {
    })
  }

  resultFound() {
    return Object.keys(this.currentWeather).length > 0;
  }
}
