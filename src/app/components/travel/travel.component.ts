import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Travel } from 'src/app/models/travel';
import { AuthService } from 'src/app/services/auth.service';
import { TravelService } from 'src/app/services/travel.service';

@Component({
  selector: 'app-travel',
  templateUrl: './travel.component.html',
  styleUrls: ['./travel.component.css']
})
export class TravelComponent implements OnInit {
  travel: Travel
  title: string
  destination: string
  startDate: string
  endDate: string
  description: string
  res: any
  userId: string
  routeSub: any
  
  constructor(private travelService: TravelService,
    private router: Router,
    private flashMessagesService: FlashMessagesService,
    private authService: AuthService,
    private route: ActivatedRoute) { }

    //Local Variable defined 
  formattedaddress=" "; 
  options={ 
  //  componentRestrictions:{ 
  //  } 
  }

  public AddressChange(address: any) { 
    if (address) {
      //setting address from API to local variable 
      this.formattedaddress=address.formatted_address 
      this.travel.destination=address.formatted_address 
    } 
  }

  ngOnInit(): void {
    this.travel = {
      _id: '',
      title: this.title,
      destination: this.destination,
      startDate: this.startDate,
      endDate: this.endDate,
      description: this.description,
      userId: this.authService.getId(),
      heroImg: ''
    }
    //this extracts the id from the url
    this.routeSub = this.route.params.subscribe(params => {
      if (params['id']) {
        this.travelService.getTravelsById(params['id']).subscribe((data: any) => {
          this.travel = data
        })
      }
    })
  }

  onTravelSubmit() {
    //Validate for title
    if (!this.travel.title) {
      this.flashMessagesService.show('Please add title',
      {cssClass: 'alert alert-dismissible alert-primary', timeout: 3000})
      return
    }
    if (!this.travel._id) {
      //Add New Travel
    this.travelService.addTravel(this.travel).subscribe(
      data => {
        this.res=data
        if (this.res.success) {
          this.flashMessagesService.show('Your travel details have been added',
          {cssClass: 'alert alert-dismissible alert-success', timeout: 3000})
          //this.router.navigateByUrl('/travel', {skipLocationChange: true}).then(()=>
          //this.router.navigate(['/dashboard']));
          this.router.navigate(['/travel/'+this.res.travel._id])
        } else {
          this.flashMessagesService.show('Your details could not be added. Try again later.',
          {cssClass: 'alert alert-dismissible alert-primary', timeout: 3000})
          this.router.navigate(['/dashboard']);
        }
      })
    } else {
      //Update Existing travel
      this.travelService.updateTravel(this.travel._id, this.travel).subscribe(
        data => {
          this.res=data
          if (this.res.success) {
            this.flashMessagesService.show('Your travel details have been updated.',
            {cssClass: 'alert alert-dismissible alert-success', timeout: 3000})
            this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
            this.router.navigate(['/dashboard']));
          } else {
            this.flashMessagesService.show('Your details could not be updated. Try again later.',
            {cssClass: 'alert alert-dismissible alert-primary', timeout: 3000})
            this.router.navigate(['/travel/'+this.travel._id]);
          }
      })
    }
  }
}
