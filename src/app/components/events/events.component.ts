import { Component, Input, OnInit } from '@angular/core';
import { EventsService } from 'src/app/services/events.service';

import { Client } from 'predicthq';
import { environment } from 'src/environments/environment';

const accessToken: string = environment.accessToken
const client = new Client({access_token: accessToken});

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
  @Input() locName: string
  constructor(private eventService: EventsService) { }
  events: []
  hqPlace

  ngOnInit(): void {
    if(this.locName) {
      this.searchEvents(this.locName)
    }
  }

  searchEvents(loc) {
    //get place id first
    client.places.search({q: loc})
      .then(
          (res) => {
            this.hqPlace = res
            let location
            for(const pl of res) {
              //get first from list and break
              location = pl.id
              break
            }
            //fetch events for place id
            client.events.search({'place.scope': location})
            .then(
                (results) => {
                  this.events = results
                }
            ).catch(
                err => console.error(err)
            );
          }
      ).catch(
        err => console.error(err)
      )
  }
}
