import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../../services/validate.service'
import { FlashMessagesService } from 'angular2-flash-messages'
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  name: String
  username: String
  email: String
  password: String
  dob: Date
  interests: String
  scribble: String
  res: any

  constructor(private validateService: ValidateService,
    private flashMessagesService: FlashMessagesService,
    private authService: AuthService,
    private router: Router) { }

  ngOnInit(): void {
  }

  onRegisterSubmit() {
    const user = {
      name: this.name,
      email: this.email,
      username: this.username,
      password: this.password,
      dob: this.dob,
      interests: this.interests,
      scribble: this.scribble
    }
    
    //Required Fields
    if (!this.validateService.validateRegister(user)) {
      this.flashMessagesService.show('Oops, you missed a required field!',
      {cssClass: 'alert-danger', timeout: 3000});
      return false      
    }

    //validate email
    if (!this.validateService.validateEmail(user.email)) {
      this.flashMessagesService.show('Check your email again!',
      {cssClass: 'alert-danger', timeout: 3000});
      return false      
    }

    //Register User
    this.authService.registerUser(user).subscribe(
      data => {
        this.res=data
        if (this.res.success) {
          this.flashMessagesService.show('You are registered now. Your username is - ' + this.username,
          {cssClass: 'alert alert-dismissible alert-success', timeout: 3000})
          this.router.navigate(['/login'])
        } else {
          this.flashMessagesService.show('You could not be registered. Please try again later. ' + this.res.msg,
          {cssClass: 'alert alert-dismissible alert-primary', timeout: 3000})
          this.router.navigate(['/register'])
        }
    })
  }
}
