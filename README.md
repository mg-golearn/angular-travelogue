# AngularSrc

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.7.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

#Run the app
Go to https://guarded-anchorage-00999.herokuapp.com/
1) On the page, click register to register yourself as a user
2) On successful registration you are taken to login page
3) enter you username (NOT name or email) and password to login. The username is case sensitive so unless you use the exact username case, this will not authenticate you
4) you will be taken to dashboard where you can add travel by adding title, destination, dates and description. Only title is mandatory here
5) On successful addition of travel you are taken to this travel specific page where you can see the upcoming events and the current weather.
6) You can also add images here
7) You can go back to dashboard on clicking the link on dashboard. The dashboard will now show your recently added travel details as travel card and you can also search travels on this page
8) While searching travels the add-travel section is not shown. When search is cleared this add-travel section appears again
9) There is a profile section which shows your info while registration. There is also a functionality to update password and there are checks for password mismatch and incorrect current password.
10) You can also update some notes in 'I scribble..' section
11) The application reads google places API to autocomplete travel destination
12) it used predicthq to get events for the destination.
13) Uses openWeather API to get weather data for a destination
14) It also uses gcp storage bucket to store uploaded images 
15) The logout link logs the user out. 
